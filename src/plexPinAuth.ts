import fetch from 'node-fetch';
import { PLEX_API_OPTIONS } from './plexClient';

interface PlexResponse {
  pin: {
    id: number;
    code: string;
    expires_at: string;
    user_id: string | null;
    client_identifier: string;
    trusted: boolean;
    auth_token: string | null;
  };
}

const HEADERS = {
  'X-Plex-Client-Identifier': PLEX_API_OPTIONS.identifier,
  'X-Plex-Product': PLEX_API_OPTIONS.product,
  'X-Plex-Version': PLEX_API_OPTIONS.version,
};

class PlexPinAuth {
  private sleep(milliseconds: number): Promise<void> {
    return new Promise((resolve) => {
      setTimeout(resolve, milliseconds);
    });
  }

  private async requestAuthCode(): Promise<PlexResponse> {
    const response = await fetch('https://plex.tv/pins.json', {
      method: 'POST',
      headers: HEADERS,
    });
    return response.json();
  }

  private async getToken(id: number): Promise<PlexResponse> {
    const response = await fetch(`https://plex.tv/pins/${id}.json`, {
      method: 'GET',
      headers: HEADERS,
    });
    return response.json();
  }

  private async waitForToken(id: number): Promise<string> {
    let response: undefined | PlexResponse;
    const start = Date.now();

    while (!response || !response.pin.auth_token) {
      response = await this.getToken(id);
      if (response.pin.auth_token) {
        return response.pin.auth_token;
      }

      const elapsedTime = Date.now() - start;
      if (elapsedTime > 120000) {
        throw new Error('Timed out');
      }
      await this.sleep(3000);
    }
    return '';
  }

  plexApi: any;
  initialize(plexApi: any) {
    this.plexApi = plexApi;
  }
  async authenticate(
    plexApi: any,
    callback: (error: Error | null, token?: string) => void,
  ) {
    try {
      const response = await this.requestAuthCode();
      console.log(
        `Visit https://plex.tv/pin and enter code ${response.pin.code} to authorise the application. The generated token will appear here once done.`,
      );
      const token = await this.waitForToken(response.pin.id);
      callback(null, token);
    } catch (e) {
      callback(e as Error);
    }
  }
}

export default new PlexPinAuth();
