import { Router } from 'express';
import {
  Gauge,
  LabelValues,
  register,
  collectDefaultMetrics,
} from 'prom-client';

import { client } from '../plexClient';
import {
  DefaultMediaContainer,
  LibraryMediaContainer,
  MediaContainer,
  MediaType,
  SessionMetadata,
} from 'plex-api';

// Init router and path
const router = Router();

async function getSession(): Promise<DefaultMediaContainer<SessionMetadata>> {
  const result = await client.query('/status/sessions');
  return result.MediaContainer;
}

async function getLibrary() {
  const response = await client.query('/library/sections');
  return response.MediaContainer;
}

async function getAll(section: number): Promise<DefaultMediaContainer> {
  const response = await client.query(`/library/sections/${section}/all`);
  return response.MediaContainer;
}

async function getAllCount(section: number) {
  const response = await getAll(section);
  return response.size;
}

async function getAlbums(section: number) {
  const response = await client.query(`/library/sections/${section}/albums`);
  return response.MediaContainer;
}

async function getAlbumCount(getAlbumsPromise: Promise<MediaContainer>) {
  const response = await getAlbumsPromise;
  return response.size;
}

async function getTrackCount(albumsResponse: Promise<DefaultMediaContainer>) {
  const response = await albumsResponse;
  return response.Metadata.reduce((cur: number, next: any) => {
    return cur + next.leafCount;
  }, 0);
}

if (process.env.NODE_ENV === 'production') {
  collectDefaultMetrics({ register });
}

/* TODO: ADD SERVER INFO TO EVERY METRIC https://github.com/siimon/prom-client#default-labels-segmented-by-registry
const defaultLabels = { serviceName: 'api-v1' };
client.register.setDefaultLabels(defaultLabels);
  */
const tuple = <T extends string[]>(...args: T) => args;

const sectionLabels = tuple('type', 'name');
type SectionLabels = typeof sectionLabels[number];
const sectionCountGauge = new Gauge<SectionLabels>({
  name: 'plex_library_section_size_count',
  help: 'Number of items within a library section',
  labelNames: sectionLabels,
});

const nowPlayingLabels = tuple(
  'type',
  'title',
  'show',
  'season',
  'episodeIdentifier',
  'artist',
  'album',
  'progress',
  'player_product',
  'player_title',
  'player_platform',
  'player_state',
);
type NowPlayingLabels = typeof nowPlayingLabels[number];
const nowPlayingGauge = new Gauge<NowPlayingLabels>({
  name: 'plex_sessions_active',
  help: 'Now playing',
  labelNames: nowPlayingLabels,
});

type MediaTypeCounts = Partial<Record<MediaType, number>>;

async function getAudioCounts(sectionId: number): Promise<MediaTypeCounts> {
  const getAlbumsPromise = getAlbums(sectionId);

  const artistPromise = getAllCount(sectionId);
  const tracksPromise = getTrackCount(getAlbumsPromise);
  const albumPromise = getAlbumCount(getAlbumsPromise);

  const [artistCount, trackCount, albumCount] = await Promise.all([
    artistPromise,
    tracksPromise,
    albumPromise,
  ]);

  return {
    artist: artistCount,
    album: albumCount,
    track: trackCount,
  };
}

// leafcount - numepisodes, childcount - series

async function getMovieCount(sectionId: number): Promise<MediaTypeCounts> {
  const count = await getAllCount(sectionId);
  return {
    movie: count,
  };
}

async function getShowCount(sectionId: number): Promise<MediaTypeCounts> {
  const all = await getAll(sectionId);
  const counts = {
    show: all.size,
    season: 0,
    episode: 0,
  };
  all.Metadata.forEach((metadata) => {
    if (metadata.childCount) {
      counts.season += metadata.childCount;
    }
    counts.episode += metadata.leafCount;
  });
  return counts;
}

async function getPhotoCount(sectionId: number): Promise<MediaTypeCounts> {
  const response = await client.query<LibraryMediaContainer>(
    `/library/sections/${sectionId}/cluster`,
  );
  const counts = {
    album: response.MediaContainer.size,
    photo: 0,
  };
  response.MediaContainer.Directory.forEach((d) => {
    counts.photo += d.size || 0;
  });
  return counts;
}

function getCount(
  type: MediaType,
  sectionId: number,
): Promise<MediaTypeCounts> {
  switch (type) {
    case 'artist':
      return getAudioCounts(sectionId);
    case 'movie':
      return getMovieCount(sectionId);
    case 'show':
      return getShowCount(sectionId);
    case 'photo':
      return getPhotoCount(sectionId);
  }

  console.log('Missing function for type', type);
  return Promise.resolve({});
}

router.get('/', (req, res) => {
  res.redirect('/metrics');
});

router.get('/metrics', async (req, res) => {
  const library = await getLibrary();
  const sessionsPromise = getSession();

  const countPromises = await library.Directory.map(async (directory) => {
    const counts: MediaTypeCounts = await getCount(
      directory.type,
      directory.key,
    );

    Object.entries(counts).forEach(([mediaType, count]) => {
      if (count == null) return;
      sectionCountGauge.set(
        {
          name: directory.title,
          type: mediaType,
        },
        count,
      );
    });
  });

  const sectionsPromise = Promise.all(countPromises);

  const nowPlayingPromise = sessionsPromise.then((sessions) => {
    if (!sessions.Metadata) {
      nowPlayingGauge.set(0);
      return;
    }
    sessions.Metadata.forEach((x) => {
      const labels: LabelValues<NowPlayingLabels> = {
        type: x.type,
        title: x.title,
        progress: x.viewOffset / x.duration,
        player_product: x.Player.product,
        player_title: x.Player.title,
        player_platform: x.Player.platform,
        player_state: x.Player.state,
      };

      if (x.type === 'episode') {
        const episodeIdentifier = `S${(x?.parentIndex || '')
          .toString()
          .padStart(2, '0')}E${x.index.toString().padStart(2, '0')}`;

        labels.show = x.grandparentTitle;
        labels.episodeIdentifier = episodeIdentifier;
      }

      if (x.type === 'track') {
        labels.artist = x.grandparentTitle;
        labels.album = x.parentTitle;
      }

      nowPlayingGauge.set(labels, sessions.size);
    });
  });

  // TODO: ERROR HANDLING, WAIT UNTIL ALL ARE DONE OR NOT
  // TODO: ADD PLEX_UP METRIC THAT IS 0 ON ERROR, 1 ON NORMAL
  await Promise.all([sectionsPromise, nowPlayingPromise]);

  const metrics = await register.metrics();
  res.set('Content-Type', register.contentType);

  // Reset the metrics in case we fail next time, are we better returning null rather than stale data?
  register.resetMetrics();
  res.end(metrics);
});

// Export the base-router
export default router;
