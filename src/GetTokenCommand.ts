import plexPinAuth from './plexPinAuth';

async function getToken() {
  return new Promise((resolve, reject) => {
    plexPinAuth.authenticate({}, (error, token) => {
      if (error) {
        reject(error);
      } else {
        console.log('Token is ' + token);
        console.log(
          'Copy the token to your config file or specify it on the command line with `--token`',
        );
        resolve(token);
      }
    });
  });
}

export default getToken;
