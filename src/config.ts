import { cosmiconfigSync } from 'cosmiconfig';
import commandLineArgs from 'command-line-args';

// Require the packagejson instead of import so that TS doesn't include it in the output
// tslint:disable-next-line:no-var-requires
const packageJson = require('../package.json');

interface PackageInfo {
  name: string;
  version: string;
}

interface Config {
  token?: string;
  username?: string;
  password?: string;
  port: number;
  plexServer: string;
  logLevel: 'info' | 'debug' | 'error' | 'warn';
  command: 'server' | 'token';
  package: PackageInfo;
}

// TODO: implement proper CLI handling for token command (e.g. no args for that command)
// https://github.com/75lb/command-line-args/wiki/Implement-command-parsing-(git-style)

// Setup command line options
const options = commandLineArgs(
  [
    {
      name: 'command',
      defaultOption: true,
      defaultValue: 'server',
    },
    {
      name: 'config.file',
      type: String,
      alias: 'c',
    },
    {
      name: 'token',
      type: String,
      alias: 't',
    },
    {
      name: 'port',
      type: Number,
      alias: 'p',
    },
    {
      name: 'plex-server',
      type: String,
      alias: 's',
    },
    {
      name: 'log-level',
      type: String,
    },
    {
      name: 'username',
      type: String,
    },
    {
      name: 'password',
      type: String,
    },
  ],
  {
    camelCase: true,
  },
);

let configResult;

const configLoader = cosmiconfigSync(packageJson.name);
if (options.configFile) {
  configResult = configLoader.load(options.configFile);
} else {
  configResult = configLoader.search();
}

// const config: Config = {
//   logLevel: options['log-level'] || configResult?.config.logLevel,
//   port: options.port || configResult?.config.port,
//   plexServer: options['plex-server'] || configResult?.config.plexServer || '',
//   token: options.token || configResult?.config.token || '',
// };

const config: Config = {
  package: {
    name: packageJson.name,
    version: packageJson.version,
  },
  port: 9594,
  logLevel: 'info',
  ...(configResult?.config || {}),
  ...options,
};

if (config.command === 'server') {
  if (!config.plexServer) {
    throw new Error(
      'plex-server must be specified either with `--plex-server` command line argument or `plexServer` config option',
    );
  }
  if (!config.token && !config.username && !config.password) {
    throw new Error(
      'No authentication info specified. Either specify a token (using the `--token` command line argument) or a username and password (`--username`, `--password`)',
    );
  }
  if (
    (config.username && !config.password) ||
    (!config.username && config.password)
  ) {
    throw new Error(
      'Received incomplete authentication information. If specifying credentials, ensure you have username and password specified',
    );
  }
}

export default config;
