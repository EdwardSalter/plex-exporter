declare module "plex-api" {
  import {
  DefaultMediaContainer,
  LibraryMediaContainer, MediaContainer,
  PlexResponse,
  SessionMetadata
} from "plex-api";

  class PlexAPI {
    constructor(options: any);
    
    query(
      relativeUrl: "/status/sessions"
    ): Promise<PlexResponse<DefaultMediaContainer<SessionMetadata>>>;
    query(
      relativeUrl: "/library/sections"
    ): Promise<PlexResponse<LibraryMediaContainer>>;
    query(relativeUrl: string): Promise<PlexResponse>;
    query<T extends MediaContainer>(relativeUrl: string): Promise<PlexResponse<T>>;
  }

  export = PlexAPI;

  namespace PlexAPI {
    export type MediaType =
      | "track"
      | "episode"
      | "artist"
      | "album"
      | "show"
      | "season"
      | "movie"
      | "photo";

    export interface PlexResponse<
      T extends MediaContainer = DefaultMediaContainer
    > {
      MediaContainer: T;
    }

    export interface MediaContainer {
      size: number;
      allowSync: boolean;
      art: string;
      identifier: string;
      mediaTagPrefix: string;
      mediaTagVersion: number;
      mixedParents: boolean;
      nocache: boolean;
      thumb: string;
      title1: string;
      title2: string;
      viewGroup: string;
      viewMode: number;
    }

    export interface DefaultMediaContainer<
      T extends Metadata = DefaultMetadata
    > extends MediaContainer {
      Metadata: T[];
    }

    export interface Metadata {
      librarySectionID: number;
      librarySectionTitle: string;
      ratingKey: string;
      key: number;
      parentRatingKey: string;
      guid: string;
      parentGuid: string;
      type: MediaType;
      title: string;
      parentKey: string;
      parentTitle: string;
      summary: string;
      index: number;
      lastViewedAt: number;
      year: number;
      thumb: string;
      parentThumb: string;
      originallyAvailableAt: Date;
      addedAt: number;
      updatedAt: number;
    }

    export interface DefaultMetadata extends Metadata {
      librarySectionUUID: string;
      allowSync: boolean;
      viewCount: number;
      leafCount: number;
      childCount?: number;
      loudnessAnalysisVersion: string;
      Genre: Genre[];
    }

    export interface Genre {
      tag: string;
    }

    export interface SessionMetadata extends Metadata {
      art: string;
      contentRating: string;
      duration: number;
      grandparentArt: string;
      grandparentGuid: string;
      grandparentKey: string;
      grandparentRatingKey: string;
      grandparentTheme: string;
      grandparentThumb: string;
      grandparentTitle: string;
      librarySectionKey: string;
      parentIndex: string;
      rating: string;
      sessionKey: string;
      viewOffset: number;
      Media: Media[];
      Director: Director[];
      Writer: Director[];
      User: User;
      Player: Player;
      Session: Session;
    }

    export interface Director {
      filter: string;
      id: string;
      tag: string;
    }

    export interface Media {
      aspectRatio: string;
      audioChannels: string;
      audioCodec: string;
      audioProfile: string;
      bitrate: string;
      container: string;
      duration: string;
      height: string;
      id: string;
      videoCodec: string;
      videoFrameRate: string;
      videoProfile: string;
      videoResolution: string;
      width: string;
      selected: boolean;
      Part: Part[];
    }

    export interface Part {
      audioProfile: string;
      container: string;
      duration: string;
      file: string;
      id: string;
      key: string;
      size: string;
      videoProfile: string;
      decision: string;
      selected: boolean;
      Stream: Stream[];
    }

    export interface Stream {
      bitDepth?: string;
      bitrate: string;
      chromaSubsampling?: string;
      codec: string;
      codedHeight?: string;
      codedWidth?: string;
      colorPrimaries?: string;
      colorRange?: string;
      colorSpace?: string;
      colorTrc?: string;
      default: string;
      displayTitle: string;
      frameRate?: string;
      height?: string;
      id: string;
      index: string;
      level?: string;
      profile: string;
      refFrames?: string;
      streamType: string;
      width?: string;
      location: string;
      audioChannelLayout?: string;
      channels?: string;
      samplingRate?: string;
      selected?: string;
    }

    export interface Player {
      address: string;
      device: string;
      machineIdentifier: string;
      model: string;
      platform: string;
      platformVersion: string;
      product: string;
      remotePublicAddress: string;
      state: string;
      title: string;
      vendor: string;
      version: string;
      local: boolean;
      relayed: boolean;
      secure: boolean;
      userID: number;
    }

    export interface Session {
      id: string;
      bandwidth: number;
      location: string;
    }

    export interface User {
      id: string;
      thumb: string;
      title: string;
    }

    export interface LibraryMediaContainer extends MediaContainer {
      Directory: Directory[];
    }

    export interface Directory {
      allowSync: boolean;
      art: string;
      composite: string;
      filters: boolean;
      refreshing: boolean;
      thumb: string;
      key: number;
      type: MediaType;
      title: string;
      agent: string;
      scanner: string;
      language: string;
      uuid: string;
      updatedAt: number;
      createdAt: number;
      scannedAt: number;
      content: boolean;
      directory: boolean;
      contentChangedAt: number;
      Location: Location[];
      enableAutoPhotoTags?: boolean;
      size?: number;
    }

    export interface Location {
      id: number;
      path: string;
    }
  }
}
